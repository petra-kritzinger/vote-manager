Vote Manager
-	By Petra Kritzinger
-	February 2017

Welcome to Vote Manager

When run for the first time, some test data will be created. New Vote topics can also be added by selecting Vote Topics > Create New. Remember to add the options as a comma separated list.
 
Always log in to be able to access the options

Click on "View Results" to choose the current voting option.
 
The list of options and their statuses will be displayed. 
Voting is now open for participants. When they click on Vote Now, the current two options will be displayed.
 
Only one vote is allowed per user.
 
The Results can be viewed by following the "View Results" option.
 
Select "Progress to next Option" to select a winner and progress to the next option. The winner of the current option will now contend with the next option in the list.
 
When all options have received a chance for voting, a winner will be announced.
