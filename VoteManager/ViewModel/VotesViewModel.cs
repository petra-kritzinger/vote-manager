﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VoteManager.DAL;
using VoteManager.Models;

namespace VoteManager.ViewModel
{
    public class VotesViewModel
    {
        private VoteManagerContext db = new VoteManagerContext();

        public VotePhase CurrentVotePhase { get; set; }
        public VoteTopic CurrentVoteTopic { get; set; }
        public bool OpenVotePhase { get; set; }
        public string ErrorText { get; set; }
        public VoteOption Option1 { get; set; }
        public VoteOption Option2 { get; set; }

        public VotesViewModel()
        {
            try
            {
                // Get Active vote phase
                OpenVotePhase = false;
                var votePhases = from b in db.VotePhases
                                 where b.ActiveInd.Equals(true)
                                 select b;
                if (votePhases == null || votePhases.Count() < 1)
                {
                    ErrorText = "No Vote Phase current";
                    return;
                }

                var voteTopics = from t in db.VoteTopics
                                 where t.OpenInd.Equals(true)
                                 select t;

                if (voteTopics == null || voteTopics.Count() < 1)
                {
                    ErrorText = "No Current Vote Topics";
                }

                CurrentVotePhase = votePhases.First();
                CurrentVoteTopic = voteTopics.First();
                var option1List = from v in db.VoteOptions
                                  where v.ID.Equals(CurrentVotePhase.VoteOption1Id)
                                  select v;
                Option1 = option1List.First();
                var option2List = from v in db.VoteOptions
                                  where v.ID.Equals(CurrentVotePhase.VoteOption2Id)
                                  select v;
                Option2 = option2List.First();
                OpenVotePhase = true;
                Option1.Status = VoteOptionStatus.InProgress;
                Option2.Status = VoteOptionStatus.InProgress;
                db.Entry(Option1).State = EntityState.Modified;
                db.Entry(Option2).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorText = ex.Message;
                
            }

        }

        internal void Vote(int option, string username)
        {
            VoteOption voteOption;
            if (option == 1)
                voteOption = Option1;
            else
                voteOption = Option2;

            voteOption.CurrentVotes++;
            Vote vote = new Vote()
            {
                UserName = username,
                VoteDate = DateTime.Now,
                VoteOption = voteOption.ID.ToString(),
                VotePhaseId = CurrentVotePhase.ID
            };
            db.Entry(voteOption).State = EntityState.Modified;
            db.Entry(vote).State = EntityState.Added;
            db.SaveChanges();
        }

        internal bool UserVotedAlready(string username)
        {
            var votes = from v in db.Votes
                        where (v.UserName.Equals(username) && v.VotePhaseId.Equals(CurrentVotePhase.ID))
                        select v;
            return (votes != null && votes.Count() > 0);
        }
    }
}