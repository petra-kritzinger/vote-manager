﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoteManager.DAL;
using VoteManager.Models;

namespace VoteManager.ViewModel
{
    public class ResultsViewModel
    {
        private VoteManagerContext db = new VoteManagerContext();

        public IEnumerable<VoteTopic> VoteTopics { get; set; }
        public IEnumerable<VoteOption> VoteOptions { get; set; }
        public List<SelectListItem> ItemsInDropDown { get; set; }
        public VoteTopic SelectedVoteTopic { get; set; }
        public string SelectedValue { get; set; }
        public bool OpenTopicExists { get; set; }
        public string ErrorText { get; set; }
        public VotePhase CurrentVotePhase { get; set; }


        public ResultsViewModel()
        {
            ItemsInDropDown = new List<SelectListItem>();
            FindCurrentVoteTopic();
        }

        private void FindCurrentVoteTopic()
        {
            VoteTopics = from t in db.VoteTopics
                         select t;
            OpenTopicExists = false;
            foreach (var topic in VoteTopics)
            {
                ItemsInDropDown.Add(new SelectListItem { Text = topic.TopicName, Selected = false, Value = topic.ID.ToString() });
                if (topic.OpenInd)
                {
                    SetSelectedTopic(topic);
                }
            }
        }

        private void SetSelectedTopic(VoteTopic topic)
        {
            SelectedVoteTopic = topic;
            VoteOptions = from b in db.VoteOptions
                          where b.VoteTopicId.Equals(SelectedVoteTopic.ID)
                          orderby b.ID
                          select b;
            OpenTopicExists = true;
        }

        internal void ProgressToNext()
        {
            try
            {
                if (CurrentVotePhase == null)
                {
                    FindCurrentVoteTopic();
                    var votephases = from p in db.VotePhases
                                     where p.ActiveInd.Equals(true)
                                     select p;
                    CurrentVotePhase = votephases.First();
                }
                

                var option1 = db.VoteOptions.Find(CurrentVotePhase.VoteOption1Id);
                var option2 = db.VoteOptions.Find(CurrentVotePhase.VoteOption2Id);
                //var option1List = from o in db.VoteOptions
                //                  where o.ID.Equals(CurrentVotePhase.VoteOption1Id)
                //                  select o;
                //var option2List = from o in db.VoteOptions
                //                  where o.ID.Equals(CurrentVotePhase.VoteOption2Id)
                //                  select o;
                //var option1 = option1List.ElementAt(0);
                //var option2 = option2List.ElementAt(0);
                int winnerId;
                string winnerName = "";
                if (option1.CurrentVotes > option2.CurrentVotes)
                {
                    option1.Status = VoteOptionStatus.Winner;
                    option1.CurrentVotes = 0;
                    option2.Status = VoteOptionStatus.Lost;
                    winnerId = option1.ID;
                    winnerName = option1.OptionName;
                }
                else if (option2.CurrentVotes > option1.CurrentVotes)
                {
                    option2.Status = VoteOptionStatus.Winner;
                    option2.CurrentVotes = 0;
                    option1.Status = VoteOptionStatus.Lost;
                    winnerId = option2.ID;
                    winnerName = option2.OptionName;
                }
                else
                {
                    ErrorText = "We have a tie! Cannot proceed to next round";
                    return;
                }


                db.Entry(option1).State = EntityState.Modified;
                db.Entry(option2).State = EntityState.Modified;
                db.SaveChanges();

                if (SelectedVoteTopic.NextVoteTopic > db.VoteOptions.Count())
                {
                    ErrorText = "We have a winner! The winner is " + winnerName;
                    return;
                }

                if (VoteOptions == null)
                    VoteOptions = from b in db.VoteOptions
                                  where b.VoteTopicId.Equals(SelectedVoteTopic.ID)
                                  orderby b.ID
                                  select b;


                else
                {

                    var nextOption = VoteOptions.ElementAt(SelectedVoteTopic.NextVoteTopic);
                    SelectedVoteTopic.NextVoteTopic++;
                    var newVotingPhase = new VotePhase()
                    {
                        ActiveInd = true,
                        DateStarted = DateTime.Now,
                        VoteOption1Id = winnerId,
                        VoteOption2Id = nextOption.ID,
                        VoteTopicId = SelectedVoteTopic.ID
                    };
                    CurrentVotePhase.ActiveInd = false;
                    db.Entry(SelectedVoteTopic).State = EntityState.Modified;
                    db.Entry(CurrentVotePhase).State = EntityState.Modified;
                    db.Entry(newVotingPhase).State = EntityState.Added;
                    db.SaveChanges();
                    CurrentVotePhase = newVotingPhase;
                }
            }
            catch (Exception ex)
            {
                ErrorText = ex.Message;
            }
        }

        private void SelectVoteTopic(VoteTopic topic)
        {
            OpenTopicExists = true;
            SelectedVoteTopic = topic;
            SetVoteOptions();
            if (VoteOptions == null || VoteOptions.Count() < 2)
            {
                ErrorText = "Not enough options to vote";
                return;
            }

                CurrentVotePhase = new VotePhase()
                {
                    DateStarted = DateTime.Now,
                    VoteOption1Id = VoteOptions.ElementAt(0).ID,
                    VoteOption2Id = VoteOptions.ElementAt(1).ID,
                    ActiveInd = true,
                    VoteTopicId = SelectedVoteTopic.ID
                };
                SelectedVoteTopic.NextVoteTopic = 2;
                SelectedVoteTopic.OpenInd = true;
                db.Entry(SelectedVoteTopic).State = EntityState.Modified;
                db.Entry(CurrentVotePhase).State = EntityState.Added;
                db.SaveChanges();

        }

        private void SetVoteOptions()
        {
            try
            {
                if (SelectedVoteTopic == null)
                {
                    ErrorText = "No Topic selected";
                    return;
                }

                var existingTopics = from e in db.VoteOptions
                                     where e.VoteTopicId.Equals(SelectedVoteTopic.ID)
                                     select e;
                if (existingTopics != null && existingTopics.Count() > 0)
                    return;
                var tempList = new List<VoteOption>();
                var voteOptions = SelectedVoteTopic.VoteOptionsCommaSeperated.Split(',').ToList();
                foreach(var option in voteOptions)
                {
                    var newOption = new VoteOption()
                    {
                        Status = VoteOptionStatus.Open,
                        OptionName = option,
                        CurrentVotes = 0,
                        VoteTopicId = SelectedVoteTopic.ID
                    };
                    db.Entry(newOption).State = EntityState.Added;
                    db.SaveChanges();
                    tempList.Add(newOption);
                }
                VoteOptions = tempList;
            }
            catch (Exception ex)
            {
                ErrorText = "An error occurred: " + ex.Message;
            }
        }

        internal void SelectCurrentTopic()
        {
            int selectedValue = 0;
            if (SelectedValue == null || !int.TryParse(SelectedValue, out selectedValue))
            {
                ErrorText = "Could not select value";
                return;
            }
            SelectVoteTopic(VoteTopics.ElementAt(selectedValue-1));
            //SelectedVoteTopic.OpenInd = true;
            //OpenTopicExists = true;
            //db.Entry(SelectedVoteTopic).State = EntityState.Modified;
            //db.SaveChanges();
            //SetVoteOptions();
        }

        internal void CloseCurrentTopic()
        {
            SelectedVoteTopic.OpenInd = false;
            OpenTopicExists = false;
            db.Entry(SelectedVoteTopic).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}