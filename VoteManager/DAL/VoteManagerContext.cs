﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using VoteManager.Models;

namespace VoteManager.DAL
{
    public class VoteManagerContext : DbContext
    {
        public VoteManagerContext() : base("VoteManagerContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public IDbSet<VoteTopic> VoteTopics { get; set; }
        public IDbSet<VotePhase> VotePhases { get; set; }
        public IDbSet<Vote> Votes { get; set; }
        public IDbSet<VoteOption> VoteOptions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            
        }

    }
}