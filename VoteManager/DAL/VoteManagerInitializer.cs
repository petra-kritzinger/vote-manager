﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VoteManager.Models;

namespace VoteManager.DAL
{
    public class VoteManagerInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<VoteManagerContext>
    {
        protected override void Seed(VoteManagerContext context)
        {

            var voteTopic = new VoteTopic
            {
                ID = 1,
                DateCreated = DateTime.Now,
                TopicName = "Programming-Language",
                Description = "Your Favourite Programming Language",
                VotesNeeded = 2,
                OpenInd = false,
                VoteOptionsCommaSeperated = "C#,Java,SQL,Python,PHP,JavaScript"
            };

            context.VoteTopics.Add(voteTopic);
            context.SaveChanges();

        }
    }
}