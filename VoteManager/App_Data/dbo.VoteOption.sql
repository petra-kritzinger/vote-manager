﻿CREATE TABLE [dbo].[VoteOption]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [OptionName] VARCHAR(128) NULL, 
    [Votes] INT NULL, 
    [Status] CHAR(1) NULL DEFAULT D
)
