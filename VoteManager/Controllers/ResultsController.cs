﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoteManager.DAL;
using VoteManager.ViewModel;

namespace VoteManager.Controllers
{
    public class ResultsController : Controller
    {
        private ResultsViewModel viewModel;

        // GET: VoteTopics
        [Authorize]
        public ActionResult Index()
        {
            if (viewModel == null)
                viewModel = new ResultsViewModel();
            if (viewModel.OpenTopicExists)
                return View("SelectedTopic", viewModel);
            else
                return View("Index", viewModel);
        }

        [HttpPost]
        public ActionResult SelectNewOpenTopic(ResultsViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    viewModel.SelectCurrentTopic();
                    return View("SelectedTopic", viewModel);
                }

                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                viewModel.ErrorText = ex.Message;
                return View(viewModel);
            }
        }

        public ActionResult SelectNewTopic(ResultsViewModel viewModel)
        {
            viewModel.CloseCurrentTopic();
            if (viewModel.OpenTopicExists)
                return View("SelectedTopic", viewModel);
            else
                return View("Index", viewModel);
        }

        public ActionResult ProgressToNext(ResultsViewModel viewModel)
        {
            viewModel.ProgressToNext();
            return View("SelectedTopic", viewModel);
        }
    }
}