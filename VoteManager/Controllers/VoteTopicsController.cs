﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VoteManager.DAL;
using VoteManager.Models;

namespace VoteManager.Controllers
{
    public class VoteTopicsController : Controller
    {
        private VoteManagerContext db = new VoteManagerContext();

        // GET: VoteTopics
        [Authorize]
        public ActionResult Index()
        {
            return View(db.VoteTopics.ToList());
        }

        // GET: VoteTopics/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VoteTopic voteTopic = db.VoteTopics.Find(id);
            if (voteTopic == null)
            {
                return HttpNotFound();
            }
            return View(voteTopic);
        }

        // GET: VoteTopics/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VoteTopics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DateCreated,DateClosed,TopicName,Description,VotesNeeded,OpenInd,VotePhaseId,NextVoteTopic,VoteOptionsCommaSeperated,Winner")] VoteTopic voteTopic)
        {
            if (ModelState.IsValid)
            {
                db.VoteTopics.Add(voteTopic);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(voteTopic);
        }

        // GET: VoteTopics/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VoteTopic voteTopic = db.VoteTopics.Find(id);
            if (voteTopic == null)
            {
                return HttpNotFound();
            }
            return View(voteTopic);
        }

        // POST: VoteTopics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DateCreated,DateClosed,TopicName,Description,VotesNeeded,OpenInd,VotePhaseId,NextVoteTopic,VoteOptionsCommaSeperated,Winner")] VoteTopic voteTopic)
        {
            if (ModelState.IsValid)
            {
                db.Entry(voteTopic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(voteTopic);
        }

        // GET: VoteTopics/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VoteTopic voteTopic = db.VoteTopics.Find(id);
            if (voteTopic == null)
            {
                return HttpNotFound();
            }
            return View(voteTopic);
        }

        // POST: VoteTopics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VoteTopic voteTopic = db.VoteTopics.Find(id);
            db.VoteTopics.Remove(voteTopic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
