﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoteManager.DAL;
using VoteManager.ViewModel;

namespace VoteManager.Controllers
{
    public class HomeController : Controller
    {
       

        public ActionResult Index()
        {
            return View();
        }

        //[Authorize]
        //public ActionResult Results()
        //{
        //    return View(new ResultsViewModel());
        //}

        [Authorize]
        public ActionResult Vote()
        {
            ViewBag.Message = "Vote Now.";

            return View();
        }

        //[Authorize]
        //public ActionResult VoteTopics()
        //{
        //    ViewBag.Message = "Vote Topics.";

        //    return View();
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}