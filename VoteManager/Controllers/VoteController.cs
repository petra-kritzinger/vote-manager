﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoteManager.ViewModel;

namespace VoteManager.Controllers
{
    public class VoteController : Controller
    {
        private VotesViewModel viewModel;

        // GET: Vote
        [Authorize]
        public ActionResult Index()
        {
            if (viewModel == null)
                    viewModel = new VotesViewModel();
            if (!viewModel.OpenVotePhase)
                return View("Error", viewModel);
            else if (viewModel.UserVotedAlready(this.User.Identity.Name))
                return View("VotedAlready", viewModel);
            else
                return View("Index", viewModel);
        }

        public ActionResult VoteOption1(VotesViewModel viewModel)
        {
            viewModel.Vote(1, this.User.Identity.Name);
            return View("Thankyou", viewModel);

        }

        public ActionResult VoteOption2(VotesViewModel viewModel)
        {
            viewModel.Vote(2, this.User.Identity.Name);
            return View("Thankyou", viewModel);

        }
    }


}