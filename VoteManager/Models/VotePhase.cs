﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteManager.Models
{
    public class VotePhase
    {
        public int ID { get; set; }
        public DateTime? DateStarted { get; set; }
        public DateTime? DateEnded { get; set; }
        public int VoteOption1Id { get; set; }
        public int VoteOption2Id { get; set; }
        public bool ActiveInd { get; set; }
        public int VoteTopicId { get; set; }
    }
}