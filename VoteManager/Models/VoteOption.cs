﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteManager.Models
{
    public enum VoteOptionStatus
    {
        Open,
        InProgress,
        Lost,
        Winner
    }

    public class VoteOption
    {
        public int ID { get; set; }
        public int VoteTopicId { get; set; }
        public string OptionName { get; set; }
        public int CurrentVotes { get; set; }
        public VoteOptionStatus Status { get; set; }
    }
}