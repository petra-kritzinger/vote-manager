﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteManager.Models
{
    public class VoteTopic
    {
        public int ID { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }
        public string TopicName { get; set; }
        public string Description { get; set; }
        public int VotesNeeded { get; set; }
        public bool OpenInd { get; set; }
        public int? VotePhaseId { get; set; }
        public int NextVoteTopic { get; set; }
        public string VoteOptionsCommaSeperated { get; set; }
        public string Winner { get; set; }
    }
}