﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteManager.Models
{
    public class Vote
    {
        public int ID { get; set; }
        public int VotePhaseId { get; set; }
        public string UserName { get; set; }
        public string VoteOption { get; set; }
        public DateTime VoteDate { get; set; }
        
    }
}